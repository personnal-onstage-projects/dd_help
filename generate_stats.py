import random

players = {
    "player1" : {
        "FOR" : 0,
        "DEX" : 0,
        "CON" : 0,
        "INT" : 0,
        "SAG" : 0,
        "CHA" : 0
    },
    "player2" : {
        "FOR" : 0,
        "DEX" : 0,
        "CON" : 0,
        "INT" : 0,
        "SAG" : 0,
        "CHA" : 0
    },
    "player3" : {
        "FOR" : 0,
        "DEX" : 0,
        "CON" : 0,
        "INT" : 0,
        "SAG" : 0,
        "CHA" : 0
    },
    "player4" : {
        "FOR" : 0,
        "DEX" : 0,
        "CON" : 0,
        "INT" : 0,
        "SAG" : 0,
        "CHA" : 0
    }
}

def generate_one_stats():
    array_dices = []
    for i in range(4):
        dice = random.randrange(1, 7)
        array_dices.append(dice)
    array_dices = sorted(array_dices, reverse=True)
    value = sum(array_dices[:3])
    return value

def generate_all_stats(player):
    for key, values in player.items():
        player[key] = generate_one_stats()
    return player

def calculate_for_players(dict_players):
    for play in dict_players:
        generate_all_stats(dict_players[play])
    return dict_players

def readable_dict(dict_players):
    for key, value in dict_players.items():
        print("The player: {} \n [".format(key))
        for val_key, val_stat in value.items():
            print("{}: {}".format(val_key, val_stat))
        print("] \n")

readable_dict(calculate_for_players(players))
